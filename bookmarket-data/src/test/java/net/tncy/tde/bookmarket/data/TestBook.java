package net.tncy.tde.bookmarket.data;

// Ce test n'est pas a faire ici dans une vraie application, mais dans le dossier book-services, on checkerait
// qu'a l'ajout d'un livre, il respecte (vu par services) les contraintes imposees

import net.tncy.bookmarket.data.Book;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

public class TestBook
{
    private static Validator validator;

    @BeforeClass
    public static void setUpClass()
    {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    public void testNull() throws Exception
    {
        Book b = new Book("ok", 10, 10, null);
        Set<ConstraintViolation<Book>> constraintViolations = validator.validate(b);
        Assert.assertEquals(0, constraintViolations.size());
    }

    @Test
    public void validateOkISBNBook()
    {
        Book b = new Book("ok", 10, 10, "12AB");
        Set<ConstraintViolation<Book>> constraintViolations = validator.validate(b);
        Assert.assertEquals(0, constraintViolations.size());
    }

    @Test
    public void unvalidateBadISBNBook()
    {
        Book b = new Book("ok", 10, 10, "12345AZERTY");
        Set<ConstraintViolation<Book>> constraintViolations = validator.validate(b);
        Assert.assertEquals(1, constraintViolations.size());
    }
}
