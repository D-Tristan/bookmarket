package net.tncy.bookmarket.data;

import net.tncy.tde.validator.ISBN;

import javax.persistence.Column;

public class Book
{
    private String name;
    private int nbPages;
    private int nbPagesRead;

    @ISBN
    @Column(name = "ISBN")
    private String isbn;

    public Book(String name, int nbPages, int nbPagesRead, String isbn)
    {
        this.name = name;
        this.nbPages = nbPages;
        this.nbPagesRead = nbPagesRead;
        this.isbn = isbn;
    }
}
